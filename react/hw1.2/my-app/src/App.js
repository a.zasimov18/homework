import './App.css';
import Modal from './modal/modal';
import { useState } from 'react';

function App() {

  const [modal, setModal] = useState({
    modal1:false,
    modal2:false
  })
  return (
    <div className="App">
      
      <button  onClick= {() => setModal({
        ...modal, modal1:true
      })}>Open first modal</button>
      <button  onClick= {() => setModal({
        ...modal, modal2:true
      })}>Open second modal</button>
      
      
      
      <Modal
      title={'Modal1 Title'}
      button1={'Ok'}
      button2 = {'Cancel'}
       isOpened={modal.modal1}
       onModalClose ={()=> setModal({...modal, modal1:false})}
       >
         <p>Once you delete this file, it won`t be possible to undo this action.</p>
         <p>Are you sure you want to delete it?</p>
         
      </Modal>
      
      
      <Modal
      title={'Modal2 Title'}
      btnStyle1= {{background: 'blue'}}
      btnStyle2 ={{background: 'black'}}
      button1={'Continue'}
      button2 = {'Exit'}
       isOpened={modal.modal2}
       onModalClose={()=> setModal({...modal, modal2:false})}
       >
         <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Recusandae dignissimos aliquam sequi quia nulla earum laborum, minus ut accusamus dolorem assumenda officiis quas nemo consectetur, cum libero distinctio rerum labore eligendi ad. Libero, aliquam accusamus itaque error quidem eius, sapiente reiciendis ea ut ipsum magnam ratione similique deleniti? Quibusdam natus magnam quo omnis totam aspernatur officia veritatis harum minus ad aut vero velit quasi beatae eligendi nemo illo, nostrum voluptatem provident earum fugit repellat eveniet eaque nam. Animi obcaecati excepturi quidem necessitatibus, tempore odio harum id possimus dolores veritatis aperiam. Iusto, tempore. Praesentium recusandae ut aperiam vitae, ipsum dolor debitis animi quidem excepturi, incidunt quam? Nulla obcaecati libero cum, optio similique fuga voluptatem fugit aut quasi aliquam, maiores veritatis voluptas hic. Vero autem nulla nihil! Quam culpa animi tempora sint doloremque vero quisquam labore officiis totam sequi omnis magnam consectetur maxime necessitatibus eum aut perferendis sit illum eius incidunt ab quos suscipit, ullam voluptatibus. Excepturi perferendis enim optio officiis beatae? Et id, consequatur ipsa maiores sed eos aut similique officia tempore culpa, itaque amet suscipit iste in architecto temporibus, fuga provident sunt delectus est doloribus magnam? Sed maiores omnis beatae obcaecati quam cupiditate suscipit incidunt quasi at rerum dolorum exercitationem non voluptate vitae, ut facere iste inventore aperiam? Dicta ex quo illum id dolor maiores temporibus molestiae, nam quidem, laboriosam aliquam numquam. Tenetur veniam sit tempore, quae voluptates veritatis totam, accusamus quidem doloremque, reiciendis rerum soluta odio quod blanditiis molestiae. Placeat laboriosam, deserunt repellendus possimus porro voluptas iste accusantium molestias. Placeat, accusamus iure, sed asperiores accusantium doloribus ipsam ipsum error aut facilis dignissimos aliquid numquam dolorem et itaque obcaecati aperiam, ab perferendis. Mollitia accusamus, neque dolores iste, odit veniam nihil commodi voluptate ratione numquam earum unde facere facilis quos praesentium porro dolor autem quidem animi minima corporis saepe. Repellendus iure architecto quis sequi possimus omnis id fugiat reiciendis voluptatibus et, placeat deleniti quod, sapiente nihil nisi quas. Aliquam, libero! Vero deserunt tempore voluptatibus perspiciatis, aspernatur dolorum magnam libero placeat eveniet totam molestias veniam similique necessitatibus ducimus accusantium quas, provident doloremque voluptas. Quos ducimus totam, porro alias modi incidunt ab voluptate unde quaerat quasi non quidem deserunt nam ad voluptatem ut distinctio architecto repellat asperiores minima! Voluptatem, modi, error omnis, repellendus exercitationem nisi perspiciatis deleniti molestiae quae velit adipisci sapiente tempore distinctio? Ea mollitia molestias id accusantium ab veniam perspiciatis assumenda vero blanditiis porro facere exercitationem eos fuga, iusto nostrum modi quibusdam magni eligendi tempore a. Repudiandae labore natus ullam eveniet accusamus qui at maiores aspernatur explicabo consequuntur deserunt, pariatur expedita quis fugit facere tenetur aut necessitatibus id hic odio. Cupiditate necessitatibus nisi error nostrum facere qui maiores praesentium voluptatum ab hic aspernatur architecto, voluptates nihil autem illum in provident! Blanditiis, consequatur impedit culpa, excepturi beatae nesciunt, ducimus magni suscipit earum minus tempora eius! Deleniti nulla esse accusamus ullam illum voluptatum, itaque sunt numquam quasi tempore, delectus similique porro, corporis necessitatibus quam? Sint quis reiciendis rerum consectetur obcaecati fugit sit maxime. Quae eius quisquam hic sapiente error nemo voluptatem similique aperiam illum, ipsum id consequuntur, ipsa sed optio voluptates odio eligendi nihil, at libero possimus harum! Officiis porro nam, corporis veniam, accusantium quaerat ratione perferendis, similique quidem unde voluptatum? Quae incidunt pariatur veniam, voluptatibus eum soluta aliquid similique alias! Saepe ducimus earum modi nemo, laudantium nulla ipsa, fuga quasi velit nam quidem facilis accusantium! Dolorem voluptate explicabo ullam quaerat eius. Cupiditate, mollitia doloribus a soluta dolorum assumenda hic velit accusamus, quibusdam eius quas! Tempore quam laborum cumque, ad eaque quae pariatur voluptatibus eligendi itaque unde perspiciatis nulla consequuntur, aut ipsam. Dolores temporibus voluptas quibusdam repudiandae, ab expedita obcaecati, inventore nisi rerum quaerat magni doloremque dicta cumque autem id maxime error animi eligendi, est omnis dignissimos! Blanditiis fugiat est repellendus officiis laborum! Tempore harum quisquam magnam modi, blanditiis eveniet sint, rem doloremque sunt temporibus dolores excepturi? Unde doloribus, sed reprehenderit qui suscipit perferendis eveniet, modi alias, minima atque animi fugiat sunt. Quam praesentium similique, incidunt id porro iusto aut fuga tempora rem, optio corporis blanditiis magnam a. Enim atque beatae neque architecto. Eligendi deleniti reiciendis nihil autem eveniet quam corporis consectetur sed tenetur provident ad, doloremque incidunt pariatur obcaecati. Recusandae autem repellendus accusantium mollitia explicabo labore quis consectetur sequi neque quibusdam rem et, dolor quaerat praesentium commodi culpa placeat nulla saepe assumenda sint? Ratione deleniti ea saepe! Ea ducimus, laudantium iste sequi ex cum commodi nobis distinctio inventore enim, voluptate recusandae accusamus eligendi, ipsa voluptatibus? Natus ut mollitia tempore quas deserunt suscipit ab voluptate sunt, perspiciatis sint temporibus repellendus omnis totam quasi perferendis, architecto laudantium labore repudiandae reiciendis delectus, in cupiditate? Voluptatem commodi possimus, saepe repudiandae asperiores necessitatibus eius! Vitae, beatae! Iure ipsam delectus corrupti est nam fugiat expedita voluptates aut dolore repellat non molestiae deleniti ea quia inventore quasi architecto, ex eligendi? Ut, id praesentium culpa, rerum repellendus sed aliquid quos, alias reiciendis iste mollitia aut ipsa ea odio itaque perspiciatis adipisci. Quisquam eligendi laborum, repellat totam animi commodi quaerat deserunt ipsum error vitae, iste at minima eveniet itaque omnis cupiditate perspiciatis exercitationem possimus quo temporibus aspernatur, doloremque earum. Quod quisquam numquam totam, est tempore incidunt, eligendi corporis nulla, velit sequi architecto officiis quibusdam laudantium quos perspiciatis impedit aliquam laborum adipisci error nostrum et eum consectetur iste. Optio fugiat quas omnis ad praesentium aut incidunt nihil. Eos qui veritatis nobis impedit dicta pariatur provident veniam voluptatibus, quod, unde, amet esse quibusdam at similique. Corrupti voluptates soluta laborum aut accusamus repellendus delectus? Nesciunt eligendi voluptatem rerum sunt impedit! Vero alias deleniti aperiam assumenda, libero id magnam ipsum laborum laudantium maiores in quas consequatur iure nisi, quibusdam dolorum fugiat officia, aliquid eaque cumque quod amet odio quasi temporibus? Magni molestias ipsam quis omnis quia. Nam perferendis voluptatum, veniam aspernatur excepturi quasi. Iste, sint! Praesentium nulla eveniet numquam officiis dolorum magnam sapiente consectetur aliquid, excepturi totam. Rem dolorum ea, omnis, nobis, eaque laboriosam aperiam magni ad quia vitae ut. Velit laudantium perferendis corrupti itaque, culpa ducimus consequuntur, pariatur saepe non nesciunt ea fugiat quos labore aperiam dolore officia inventore, architecto possimus? Ratione, aspernatur sint.
         </p>
       </Modal>
    </div>
  );
}

export default App;
