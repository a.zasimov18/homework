import { useState } from 'react';
import './App.css';
import Modal from "./Modal/Modal";

function App() {
  const [modalActive, setModalActive] = useState();
  return (
    <div className= "app">
      <div className="main">
        <div className="open_btn"><button  onClick={()=> setModalActive(true)}>Modal First</button></div>
      <div className="open_btn"><button  onClick={()=> setModalActive(true)}>Modal Second</button></div>
      </div>
      
    
      <Modal active = {modalActive} setActive= {setModalActive}>
        <p>Hello</p>
      </Modal>
      <Modal active = {modalActive} setActive= {setModalActive}>
        <p>bye</p>
      </Modal>
    
    
    </div>
    
    
  );
}

export default App;
