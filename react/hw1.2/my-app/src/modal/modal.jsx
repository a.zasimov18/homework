
const Modal = props => {
    return(
        <div className={`modal_wrapper ${props.isOpened ? 'open' : 'close'}`}  onClick= {props.onModalClose}>
            <div className='modal_body' onClick= {e => e.stopPropagation()}>
                <div className='modal_title'>
                <h2>{props.title}</h2>
                <div className='modal_close' onClick={props.onModalClose}>×</div>
                </div>
                <div className='modal_content'>
                    <div className=''>
                    
                    
                    </div> 
                {props.children}
                <button style={props.btnStyle1}>{props.button1}</button>
                <button style={props.btnStyle2} onClick= {props.onModalClose}>{props.button2}</button>
                </div>
                
            </div>
        </div>
    )
}
export default Modal;